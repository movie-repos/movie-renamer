from argparse import ArgumentParser
import logging
import os
import re
import pathlib
from collections import defaultdict

from video_info import VideoType, MovieInfo, ShowInfo


# Set up the Python logger for this module.
logging.basicConfig(format="[%(levelname)s] %(message)s")
logger = logging.getLogger(__name__)


def parse_args():
    parser = ArgumentParser()
    parser.add_argument(
        "sources",
        nargs="+",
        help="The source directories that contain the video files to rename.",
    )
    parser.add_argument(
        "--movies",
        "-m",
        help="Destination directory to move the movie files to. "
        "Provide - if you want to rename them in place.",
        type=str,
        default=None,
    )
    parser.add_argument(
        "--shows",
        "-s",
        help="Destination directory to move the show files to."
        "Provide - if you want to rename them in place.",
        type=str,
        default=None,
    )

    parser.add_argument(
        "--verbose",
        "-v",
        type=int,
        choices=range(1, 6),
        default=2,
        help="Set the verbosity in range [1, 5] with 1 being the noisiest. Defaults to 2.",
    )

    # Additional options.
    parser.add_argument(
        "--recurse",
        "-r",
        help="Recursively rename videos in all sub directories.",
        action="store_true",
    )
    parser.add_argument(
        "-i",
        "--interactive",
        help="Run the script interactively, prompting on every file rename.",
        action="store_true",
    )
    parser.add_argument(
        "--dry-run",
        help="Only display details of the renaming, don't change anything.",
        action="store_true",
    )

    # Provide ability to only match on a specific subset of files
    parser.add_argument(
        "--match-regex",
        help="Only process the files that match the specific regex pattern.",
        type=str,
    )

    # Provide ability to rename some files, eg. to remove the year
    parser.add_argument(
        "--rename-regex",
        help="Rename files based of a specific regex pattern.",
        nargs=2,
        type=str,
    )

    return parser.parse_args()


def validate_args(args):
    for source in args.sources:
        if not os.path.isdir(source):
            print("No such source directory '" + source + "' exists.")
            exit(1)

    if (
        args.movies is not None
        and args.movies != "-"
        and not os.path.isdir(args.movies)
    ):
        print("No such destination directory '" + args.movies + "' exists.")
        exit(1)

    if args.shows is not None and args.shows != "-" and not os.path.isdir(args.shows):
        print("No such destination directory '" + args.shows + "' exists.")
        exit(1)


def gen_videos(roots, depth=0):
    """
    Generate all video filepaths under the root directories.
    This will recurse depth times. If you want to recurse infinitely then pass -1.
    """
    for root in roots:
        for filename in os.listdir(root):
            filepath = os.path.join(root, filename)
            if (
                os.path.isfile(filepath)
                and VideoType.get(filepath) != VideoType.UNKNOWN
            ):
                yield filepath
            elif os.path.isdir(filepath) and depth != 0:
                for result in gen_videos([filepath], depth=depth - 1):
                    yield result


def get_video(filepath):
    video_type = VideoType.get(filepath)
    if video_type == VideoType.MOVIE:
        return MovieInfo(filepath)
    elif video_type == VideoType.SHOW:
        return ShowInfo(filepath)
    elif video_type == VideoType.UNKNOWN_VIDEO:
        return None
    raise ValueError("Unrecognized video file: " + filepath)


def prompt(message):
    return input(message + " [Y/n] ").lower() == "y"


class ShowStats:
    def __init__(self):
        self._seasons = defaultdict(int)

    def add(self, show_info):
        self._seasons[show_info.season] += 1

    def __str__(self):
        season_count = len(self._seasons)
        episode_count = sum(self._seasons.values())
        return f"{season_count:2d} seasons, {episode_count:3d} episodes"

    def __repr__(self):
        return str(self)


class RunStats:
    """
    Aggregate stats from a given run based on the video type.
    """

    def __init__(self):
        self._types = set()
        self._total = 0
        self._recognized = defaultdict(int)
        self._renamed = defaultdict(int)
        self._skipped = defaultdict(int)
        self._failed = defaultdict(int)
        self._show_stats = defaultdict(ShowStats)

    def add_total(self):
        self._total += 1

    def add_recognized(self, video_type, video_info):
        self._types.add(video_type)
        self._recognized[video_type] += 1
        if video_type == VideoType.SHOW:
            self._show_stats[video_info.name].add(video_info)

    def add_renamed(self, video_type):
        self._types.add(video_type)
        self._renamed[video_type] += 1

    def add_skipped(self, video_type):
        self._types.add(video_type)
        self._skipped[video_type] += 1

    def add_failed(self, video_type):
        self._types.add(video_type)
        self._failed[video_type] += 1

    def get_total(self):
        return self._total

    def get_recognized(self, video_type):
        return self._recognized.get(video_type, 0)

    def get_renamed(self, video_type):
        return self._renamed.get(video_type, 0)

    def get_skipped(self, video_type):
        return self._skipped.get(video_type, 0)

    def get_failed(self, video_type):
        return self._failed.get(video_type, 0)

    def __str__(self):
        result = f"Total: {self._total}"
        for video_type in self._types:
            result += f"\n{video_type}"
            result += f"\n  Recognized:  {self.get_recognized(video_type)}"
            result += f"\n  Renamed: {self.get_renamed(video_type)}"
            result += f"\n  Skipped: {self.get_skipped(video_type)}"
            result += f"\n  Failed: {self.get_failed(video_type)}"
            if video_type == VideoType.SHOW:
                for show_name, show_stats in self._show_stats.items():
                    if show_name is None:
                        show_name = "<UNKNOWN>"
                    result += f"\n    {show_name:>40s} {show_stats}"
            result += "\n------------------------------------------------------------------------------"
        return result

    def __repr__(self):
        return str(self)


if __name__ == "__main__":
    # Get the CLI arguments and validate them.
    args = parse_args()
    validate_args(args)

    # Print all DEBUG messages if interactive.
    if args.interactive:
        args.verbose = 1

    logger.setLevel(args.verbose * 10)

    # Record some stats for displaying after.
    stats = RunStats()

    # Generate all the video filenames and rename each one.
    for filepath in gen_videos(args.sources, depth=-1 if args.recurse else 0):
        stats.add_total()
        video = get_video(filepath)

        # Check if the video file is None, so no matching worked.
        if video is None:
            logger.error("Failed to match video type: " + filepath)
            stats.add_failed(VideoType.UNKNOWN_VIDEO)
            continue

        # Skip if a match regex was given and this video name does not match.
        if args.match_regex and not re.match(args.match_regex, video.name):
            stats.add_skipped(video.get_type())
            continue

        # Optionally rename the video(s) if they match the regex.
        # We do this before add_recognized so that the proper renamed
        # file is recorded in the stats.
        if args.rename_regex:
            old_name = video.name
            video.rename(args.rename_regex[0], args.rename_regex[1])
            logger.debug(f"Renaming from '{old_name}' to '{video.name}'")

        stats.add_recognized(video.get_type(), video)

        # Skip videos if the user hasn't specified a directory for them.
        if not args.dry_run:
            if video.type == VideoType.MOVIE and not args.movies:
                continue
            elif video.type == VideoType.SHOW and not args.shows:
                continue

        dest_dir = (
            args.movies
            if video.type == VideoType.MOVIE
            else args.shows
            if video.type == VideoType.SHOW
            else None
        )
        destination = None

        # Only consider files for which we plan on renaming.
        if dest_dir is not None:
            destination = video.get_destination(dest_dir if dest_dir != "-" else None)

            logger.debug("Source:      " + video.filepath)
            logger.debug("  Renamed:     " + video.get_name())
            logger.debug("  Video type:  " + video.type.name)
            logger.debug("  Destination: " + destination)

        rename = destination and not args.dry_run

        if args.interactive and not prompt("Rename video?"):
            rename = False

        if rename:
            try:
                pathlib.Path(os.path.dirname(destination)).mkdir(
                    parents=True, exist_ok=True
                )

                if os.path.isfile(destination):
                    logger.warning(f"File already exists, skipping: {filepath}")
                    stats.add_skipped(video.get_type())
                else:
                    os.rename(video.filepath, destination)
                    stats.add_renamed(video.get_type())
            except Exception as e:
                logger.error(str(e) + " for filepath " + filepath)
                stats.add_failed(video.get_type())
                continue

    # Print stats from the run.
    for stats_line in str(stats).splitlines():
        logger.info(stats_line)
