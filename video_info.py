import os, re
from enum import Enum


VIDEO_EXTENSIONS = ["avi", "flv", "mkv", "wmv", "mov", "mp4", "ogv"]


class RegexHelpers:
    SEPARATORS = r"\. -"  # Allowed string separators.
    PUNCTUATION = r"!',"  # Allowed punctuation
    WORD = r"\da-zA-Z" + PUNCTUATION
    WORDS = WORD + SEPARATORS  # Multiple words matching.

    @classmethod
    def strip(cls, string):
        if string is None:
            return None

        # The string must start with a letter or number.
        m = re.match(r"[^a-zA-Z0-9]*(.+)", string)
        if not m:
            return None

        string = m.group(1)

        # If there are any spaces in the string, assume that is the
        # separator and leave in other symbols.
        separators = (".", " ", "-")
        if " " in string:
            separators = " "

        # Otherwise remove all whitespace and replace with spaces.
        return " ".join(re.split("|".join(map(re.escape, separators)), string)).strip()


class VideoType(Enum):
    UNKNOWN = 0
    UNKNOWN_VIDEO = 1
    MOVIE = 2
    SHOW = 3

    @classmethod
    def get(cls, filepath):
        filename, ext = os.path.splitext(os.path.basename(filepath))

        # Remove leading period.
        ext = ext[1:]

        if ext not in VIDEO_EXTENSIONS or "sample" in filename:
            return cls.UNKNOWN
        elif MovieInfo.is_movie(filename):
            return cls.MOVIE
        elif ShowInfo.is_show(filename):
            return cls.SHOW
        return cls.UNKNOWN_VIDEO


class FileBase:
    def get_type(self):
        raise NotImplementedError


class VideoInfo(FileBase):
    LOWER_WORDS = ["the", "a", "an", "and", "or", "nor", "for", "of", "to", "in"]

    def __init__(self, filepath):
        self.type = VideoType.get(filepath)
        self.filepath = filepath
        self.filename, self.extension = os.path.splitext(os.path.basename(filepath))

    def get_name(self):
        raise NotImplementedError()

    def get_destination(self, destination):
        raise NotImplementedError()

    def capitalize(self, string):
        """ Capitalizes the first letter of each token if they aren't conjunctions """
        if string is None:
            return None
        tokens = string.split()
        return " ".join(
            (
                token[0].upper() + token[1:]
                if token.lower() not in VideoInfo.LOWER_WORDS or token == tokens[0]
                else token.lower()
            )
            for token in tokens
        )

    def strip_trailing_all_caps(self, string):
        """ Removes all trailing tokens that are all CAPS """
        if string is None:
            return None

        tokens = string.split()
        length = len(tokens)
        for i in range(length - 1, 0, -1):
            if (
                not all(c.isdigit() for c in tokens[i])
                and tokens[i].upper() == tokens[i]
            ):
                tokens = tokens[:-1]
            else:
                break
        return " ".join(tokens)

    def rename(self, regex, replacement):
        self.name = re.sub(regex, replacement, self.name)


class MovieInfo(VideoInfo):
    # Produces (name, year)
    REGEX = r"([{words}]+(\d+[{seps}][{words}]*?)?)[(]?((19|20)\d\d)[)]?".format(
        words=RegexHelpers.WORDS, seps=RegexHelpers.SEPARATORS
    )
    NOT_SHOW_RE = r".*[Ss](\d\d)([Ee]|[{seps}])(\d\d).*".format(
        seps=RegexHelpers.SEPARATORS
    )

    @classmethod
    def is_movie(cls, filename):
        return not re.match(cls.NOT_SHOW_RE, filename) and re.match(cls.REGEX, filename)

    def __init__(self, filepath):
        super().__init__(filepath)
        assert self.type == VideoType.MOVIE, "Bad call to MovieInfo() with " + filepath

        m = re.match(MovieInfo.REGEX, self.filename)
        self.name = (
            self.strip_trailing_all_caps(
                self.capitalize(RegexHelpers.strip(m.group(1)))
            )
            or None
        )
        self.year = int(m.group(3))

    def get_name(self):
        return f"{self.name} ({self.year}){self.extension}"

    def get_destination(self, destination):
        if destination is None:
            destination = os.path.dirname(self.filepath)
        return os.path.join(destination, self.get_name())

    def get_type(self):
        return VideoType.MOVIE


class ShowInfo(VideoInfo):
    # This could produce false positives on movies, but should be fine so
    # long as it's run after the movie regex.
    REGEX = r"(?P<show>[{words}\(\)]+[{seps}])?(\(\d\d\d\d\)[{seps}]+)?[Ss]?(?P<season>\d\d)[{seps}]?([Ee]|[{seps}])(?P<episode>\d\d)(?P<episode_name>[{words}]+)?".format(
        words=RegexHelpers.WORDS, seps=RegexHelpers.SEPARATORS
    )
    # Used to strip everything after the quality if it exists.
    QUALITY_RE = r".*[{seps}\()](\d\d\d+p)[{seps}\)].*".format(
        seps=RegexHelpers.SEPARATORS
    )

    @classmethod
    def is_show(cls, filename):
        filename = filename.replace(" - ", " ")
        return re.match(cls.REGEX, filename) is not None

    def __init__(self, filepath):
        super().__init__(filepath)
        assert self.type == VideoType.SHOW, "Bad call to ShowInfo() with " + filepath

        filename = self.filename
        filename = filename.replace(" - ", " ")

        # First strip stuff past the quality if it exists.
        m = re.match(ShowInfo.QUALITY_RE, filename)
        if m:
            filename = filename[: filename.find(m.group(1))]

        m = re.match(ShowInfo.REGEX, filename)
        self.name = self.capitalize(
            (RegexHelpers.strip(m.group("show")) if "show" in m.groupdict() else None)
            or self._get_show_name_from_directory()
            or None
        )

        self.season = int(m.group("season"))
        self.episode = int(m.group("episode"))
        self.episode_name = (
            self.capitalize(RegexHelpers.strip(m.group("episode_name"))) or None
        )

        # Sometimes tokens are caught at the end that are all caps, strip those out.
        self.episode_name = self.strip_trailing_all_caps(self.episode_name)

    def get_name(self):
        name = ""
        if self.name:
            name += self.name + " "
        name += f"S{self.season:02}E{self.episode:02}"
        if self.episode_name:
            name += " " + self.episode_name
        return name + self.extension

    def get_destination(self, destination):
        """
        If we know the show name, it will return:
            <destination>/<show_name>/Season <number>/<name>
        Otherwise:
            <destination>/<name>
        """
        if self.name is not None and destination is not None:
            filepath = os.path.join(self.name, f"Season {self.season}", self.get_name())
        else:
            filepath = self.get_name()

        if destination is None:
            destination = os.path.dirname(self.filepath)

        return os.path.join(destination, filepath)

    def _get_show_name_from_directory(self):
        """ Attempts to get the show name from the source directory """

        base_dir = os.path.basename(os.path.dirname(self.filepath))
        directory = self._strip_season_n(base_dir)

        # If the directory is now empty, then let's try going up one more level.
        if not directory:
            base_dir = os.path.basename(os.path.dirname(base_dir))
            directory = self._strip_season_n(base_dir)

        # Now extract the show name from the resulting trimmed directory name.
        name = re.match(
            r"([{words}]+)?.*".format(words=RegexHelpers.WORDS), directory
        ).group(1)
        if name:
            name = name.strip()
        return name

    def _strip_season_n(self, string):
        # If the string contains "Season N", strip that out.
        m = re.match(
            r".*([{seps}]?season[{seps}]\d+).*".format(seps=RegexHelpers.SEPARATORS),
            string.lower(),
        )
        if m:
            return string[: string.lower().find(m.group(1))].strip()
        return string

    def get_type(self):
        return VideoType.SHOW
