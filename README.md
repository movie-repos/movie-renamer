# Movie Renamer

This is a command-line tool for batch renaming and moving torrented movies and TV shows.

# Running

To run the script, perform the following which will display the arguments:

```sh
$ python rename.py --help
```