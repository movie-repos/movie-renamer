import unittest

from unittest_data_provider import data_provider
from video_info import VideoType, MovieInfo, ShowInfo


# Run with `python3 -m unittest tests.renamer_test` in ./movie-renamer/


class TestStringParsing(unittest.TestCase):
    """ Tests string matching and renaming """

    # List of tuples (filename, name, year, renamed)
    movies = lambda: (
        (
            "Dark.Phoenix.2019.1080p.BluRay.x264-[YTS.LT].mp4",
            "Dark Phoenix",
            2019,
            "Dark Phoenix (2019).mp4",
        ),
        (
            "El.Camino.A.Breaking.Bad.Movie.2019.1080p.NF.WEB-DL.DDP5.1.H264-CMRG.mkv",
            "El Camino a Breaking Bad Movie",
            2019,
            "El Camino a Breaking Bad Movie (2019).mkv",
        ),
        (
            "Avengers.Endgame.2019.1080p.BluRay.x264-[YTS.LT].mp4",
            "Avengers Endgame",
            2019,
            "Avengers Endgame (2019).mp4",
        ),
        (
            "John.Wick.3.2019.1080p.Bluray.1600MB.DD5.1.x264-GalaxyRG.mkv",
            "John Wick 3",
            2019,
            "John Wick 3 (2019).mkv",
        ),
        (
            "Godzilla.King.Of.The.Monsters.2019.1080p.WEBRip.x264-[YTS.LT].mp4",
            "Godzilla King of the Monsters",
            2019,
            "Godzilla King of the Monsters (2019).mp4",
        ),
        (
            "12.Angry.Men.1957.720p.x264-[YTS.LT].mp4",
            "12 Angry Men",
            1957,
            "12 Angry Men (1957).mp4",
        ),
        (
            "300.2006.1080p.1600MB.DD5.1.x264-GalaxyRG.mp4",
            "300",
            2006,
            "300 (2006).mp4",
        ),
        (
            "Five.feet.apart.2019.1080p-dual-lat-cinecalidad.is.mp4",
            "Five Feet Apart",
            2019,
            "Five Feet Apart (2019).mp4",
        ),
        ("War Machine (2017).mp4", "War Machine", 2017, "War Machine (2017).mp4",),
    )

    # List of tuples (filename, show, season, episode, episode_name, renamed)
    shows = lambda: (
        (
            "Heroes S01E01 Genesis (1080p x265 Joy).mkv",
            "Heroes",
            1,
            1,
            "Genesis",
            "Heroes S01E01 Genesis.mkv",
        ),
        (
            "Heroes Season 4 (1080p x265 Joy)/S04E19 Brave New World (1080p x265 Joy).mkv",
            "Heroes",
            4,
            19,
            "Brave New World",
            "Heroes S04E19 Brave New World.mkv",
        ),
        (
            "Heroes S03E23 1961 (1080p x265 Joy).mkv",
            "Heroes",
            3,
            23,
            "1961",
            "Heroes S03E23 1961.mkv",
        ),
        (
            "Adventure.time.S06E16.josh.and.margaret.investigations.mp4",
            "Adventure Time",
            6,
            16,
            "Josh and Margaret Investigations",
            "Adventure Time S06E16 Josh and Margaret Investigations.mp4",
        ),
        (
            "Bob's Burgers S04E22.mkv",
            "Bob's Burgers",
            4,
            22,
            None,
            "Bob's Burgers S04E22.mkv",
        ),
        (
            "02.02 - A Going Concern.mp4",
            None,
            2,
            2,
            "A Going Concern",
            "S02E02 A Going Concern.mp4",
        ),
        (
            "S02E10- The Son of Gotham.mkv",
            None,
            2,
            10,
            "The Son of Gotham",
            "S02E10 The Son of Gotham.mkv",
        ),
        (
            "Mind.Field.S01E06.Touch.2160p.RED.WEBRip.AAC5.1.VP9-BTW.mkv",
            "Mind Field",
            1,
            6,
            "Touch",
            "Mind Field S01E06 Touch.mkv",
        ),
        (
            "Rick and Morty S01E04  M. Night Shaym-Aliens! (Uncensored) (1920x1080) [Phr0stY].mkv",
            "Rick and Morty",
            1,
            4,
            "M. Night Shaym-Aliens!",
            "Rick and Morty S01E04 M. Night Shaym-Aliens!.mkv",
        ),
        (
            "Silicon.Valley.S02E08.720p.BluRay.x264.ShAaNiG.mkv",
            "Silicon Valley",
            2,
            8,
            None,
            "Silicon Valley S02E08.mkv",
        ),
        (
            "The Boys - S02E01 - The Big Ride.mkv",
            "The Boys",
            2,
            1,
            "The Big Ride",
            "The Boys S02E01 The Big Ride.mkv",
        ),
        (
            "The Boys - S02 E01 - The Big Ride.mkv",
            "The Boys",
            2,
            1,
            "The Big Ride",
            "The Boys S02E01 The Big Ride.mkv",
        ),
        (
            "The EXPANSE - S05 E06 - Tribes (1080p - AMZN WebRip).mp4",
            "The EXPANSE",
            5,
            6,
            "Tribes",
            "The EXPANSE S05E06 Tribes.mp4",
        ),
    )

    @data_provider(movies)
    def test_movie(self, filename, name, year, renamed):
        self.assertEqual(VideoType.get(filename), VideoType.MOVIE, filename)

        movie = MovieInfo(filename)
        self.assertEqual(movie.name, name, filename)
        self.assertEqual(movie.year, year, filename)
        self.assertEqual(movie.get_name(), renamed, filename)

        # We should also expect the renamed file to be identified with the same metadata.
        renamed_movie = MovieInfo(movie.get_name())
        self.assertEqual(renamed_movie.get_name(), renamed)
        self.assertEqual(renamed_movie.name, name, movie.get_name())
        self.assertEqual(renamed_movie.year, year, movie.get_name())

    @data_provider(shows)
    def test_show(self, filename, name, season, episode, episode_name, renamed):
        self.assertEqual(VideoType.get(filename), VideoType.SHOW, filename)

        show = ShowInfo(filename)
        self.assertEqual(show.name, name, filename)
        self.assertEqual(show.season, season, filename)
        self.assertEqual(show.episode, episode, filename)
        self.assertEqual(show.episode_name, episode_name, filename)
        self.assertEqual(show.get_name(), renamed, filename)

        # We should also expect the renamed file to be identified with the same metadata.
        renamed_show = ShowInfo(show.get_name())
        self.assertEqual(renamed_show.get_name(), renamed)
        self.assertEqual(renamed_show.name, name, show.get_name())
        self.assertEqual(renamed_show.season, season, show.get_name())
        self.assertEqual(renamed_show.episode, episode, show.get_name())
        self.assertEqual(renamed_show.episode_name, episode_name, show.get_name())


if __name__ == "__main__":
    unittest.main()
